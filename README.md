The LG G5 (codenamed _"h1"_) is a high-range smartphone from LG.

It was announced and released in April 2016.

## Device specifications

| Device       | LG G5                                              |
| -----------: | :----------------------------------------------    |
| SoC          | Qualcomm MSM8996 Snapdragon 820 (14 nm)            |
| CPU          | Quad-core (2x2.15 GHz Kryo & 2x1.6 GHz Kryo)       |
| GPU          | Adreno 530                                         |
| Memory       | 4GB (LPDDR3X)                                      |
| Shipped Android version | 6.0.1                                   |
| Storage      | 32GB UFS 2.0                                       |
| Battery      | Removable Li-Ion 2800 mAh, QC 3.0                   |
| Dimensions   | 149.4 x 73.9 x 7.7 mm                              |
| Display      | 2560 x 1440 (16:9), 5.3 inches                     |
| Rear camera 1 | 16 MP, f/1.8, 26mm (wide), 1/2.6", Laser AF, 3-axis OIS |
| Rear camera 2 | 8 MP, f/2.4, 9mm, 1/3.6", no AF                   |
| Front camera  | 8 MP, f/2.0, 1/4", 1.12µm                         |

## Device picture

![LG G5](https://images-na.ssl-images-amazon.com/images/I/619JNxM2frL._AC_SX679_.jpg)
